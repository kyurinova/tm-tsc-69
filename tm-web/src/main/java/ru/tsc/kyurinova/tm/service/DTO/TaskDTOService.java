package ru.tsc.kyurinova.tm.service.DTO;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kyurinova.tm.api.service.dto.ITaskDTOService;
import ru.tsc.kyurinova.tm.dto.model.TaskDTO;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.exeption.empty.EmptyFieldException;
import ru.tsc.kyurinova.tm.exeption.empty.EmptyIdException;
import ru.tsc.kyurinova.tm.exeption.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.repository.DTO.TaskDTORepository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class TaskDTOService implements ITaskDTOService {

    @Nullable
    @Autowired
    private TaskDTORepository repository;

    @Override
    @Transactional
    public @NotNull TaskDTO add(@NotNull TaskDTO model) throws Exception {
        return repository.save(model);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO addByUserId(@Nullable final String userId, @NotNull final TaskDTO model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyFieldException("UserId");
        model.setUserId(userId);
        return repository.save(model);
    }

    @Override
    @Transactional
    public void changeTaskStatusById(
            @Nullable final String id,
            @Nullable final Status status
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyFieldException("Status");
        @Nullable final TaskDTO task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        repository.save(task);
    }

    @Override
    @Transactional
    public void changeTaskStatusByUserIdAndId(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyFieldException("UserId");
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyFieldException("Status");
        @Nullable final TaskDTO task = findOneByUserIdAndId(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        repository.save(task);
    }

    @Override
    @Transactional
    public void clear() throws Exception {
        repository.deleteAll();
    }

    @Override
    @Transactional
    public void clearByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyFieldException("UserId");
        repository.deleteByUserId(userId);
    }

    @Override
    public int count() throws Exception {
        return (int) repository.count();
    }

    @Override
    public int countByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyFieldException("UserId");
        return (int) repository.countByUserId(userId);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO create(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyFieldException("UserId");
        if (name == null || name.isEmpty()) throw new EmptyFieldException("Name");
        @NotNull TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        return repository.save(task);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyFieldException("UserId");
        if (name == null || name.isEmpty()) throw new EmptyFieldException("Name");
        if (description == null || description.isEmpty()) throw new EmptyFieldException("Description");
        @NotNull TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        return repository.save(task);
    }

    @Override
    public boolean existsById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    public boolean existsByUserIdAndId(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyFieldException("UserId");
        if (id == null || id.isEmpty()) return false;
        return repository.existByUserIdAndId(userId, id);
    }


    @Override
    public @Nullable List<TaskDTO> findAll() throws Exception {
        return repository.findAll();
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyFieldException("UserId");
        return repository.findByUserId(userId);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByProjectId(@Nullable String projectId
    ) throws Exception {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findByProjectId(projectId);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByUserIdAndProjectId(@Nullable final String userId, @Nullable String projectId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyFieldException("UserId");
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findByUserIdAndProjectId(userId, projectId);
    }

    @Override
    public @Nullable TaskDTO findOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        @NotNull final Optional<TaskDTO> result = repository.findById(id);
        return result.orElse(null);
    }

    @Nullable
    @Override
    public TaskDTO findOneByUserIdAndId(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyFieldException("UserId");
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Optional<TaskDTO> result = repository.findByUserIdAndId(userId, id);
        return result.orElse(null);
    }

    @Override
    @Transactional
    public void remove(@Nullable TaskDTO model) throws Exception {
        if (model == null) throw new TaskNotFoundException();
        repository.delete(model);
    }

    @Override
    @Transactional
    public void removeByUserId(@Nullable final String userId, @Nullable final TaskDTO model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyFieldException("UserId");
        if (model == null) return;
        if (findOneByUserIdAndId(model.getUserId(), model.getId()) == null) return;
        repository.delete(model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException("Task");
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public void removeByUserIdAndId(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyFieldException("UserId");
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void changeTaskDTOStatusById(@Nullable String id, @Nullable Status status) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException("Task");
        if (status == null) throw new EmptyFieldException("Status");
        @Nullable final TaskDTO task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        repository.save(task);
    }

    @Nullable
    @Override
    @Transactional
    public TaskDTO update(@Nullable final TaskDTO model) throws Exception {
        if (model == null) return null;
        return repository.save(model);
    }

    @Nullable
    @Override
    @Transactional
    public TaskDTO updateByUserId(@Nullable final String userId, @Nullable final TaskDTO model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyFieldException("UserId");
        if (model == null) return null;
        if (findOneByUserIdAndId(model.getUserId(), model.getId()) == null) return null;
        model.setUserId(userId);
        repository.save(model);
        return model;
    }

    @Nullable
    @Override
    @Transactional
    public TaskDTO updateById(
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyFieldException("Name");
        if (description == null || description.isEmpty()) throw new EmptyFieldException("Description");
        @Nullable final TaskDTO task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        repository.save(task);
        return task;
    }

    @Nullable
    @Override
    @Transactional
    public TaskDTO updateByUserIdAndId(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyFieldException("UserId");
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyFieldException("Name");
        if (description == null || description.isEmpty()) throw new EmptyFieldException("Description");
        @Nullable final TaskDTO task = findOneByUserIdAndId(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        repository.save(task);
        return task;
    }

    @Override
    @Transactional
    public void updateProjectIdById(
            @Nullable final String id,
            @Nullable final String projectId
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final TaskDTO task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        repository.save(task);
    }

    @Override
    @Transactional
    public void updateProjectIdByUserIdAndId(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String projectId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyFieldException("UserId");
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final TaskDTO task = findOneByUserIdAndId(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        repository.save(task);
    }

}
