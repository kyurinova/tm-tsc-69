package ru.tsc.kyurinova.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.tsc.kyurinova.tm.model.CustomUser;

public final class UserUtil {

    private UserUtil() {
    }

    public static String getUserId() {
        @NotNull final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        @Nullable final Object principal = authentication.getPrincipal();
        if (principal == null) throw new AccessDeniedException("");
        if (!(principal instanceof CustomUser)) throw new AccessDeniedException("");
        @NotNull final CustomUser customUser = (CustomUser) principal;
        return customUser.getUserId();
    }

}


