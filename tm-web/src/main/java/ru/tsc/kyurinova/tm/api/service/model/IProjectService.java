package ru.tsc.kyurinova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.model.Project;

import java.util.List;

public interface IProjectService {

    @NotNull
    Project add(@NotNull Project model) throws Exception;

    @NotNull
    Project addByUserId(@Nullable String userId, @NotNull Project model) throws Exception;

    void changeProjectStatusById(
            @Nullable String id,
            @Nullable Status status
    ) throws Exception;

    void changeProjectStatusByUserIdAndId(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws Exception;


    void clear() throws Exception;

    void clearByUserId(@Nullable String userId) throws Exception;

    @NotNull
    Project create(@Nullable String userId, @Nullable String name) throws Exception;

    @NotNull
    Project create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    boolean existsById(@Nullable String id) throws Exception;

    boolean existsByUserIdAndId(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    List<Project> findAll() throws Exception;

    @Nullable
    List<Project> findAllByUserId(@Nullable String userId) throws Exception;

    @Nullable
    Project findOneById(@Nullable String id) throws Exception;

    @Nullable
    Project findOneByUserIdAndId(@Nullable String userId, @Nullable String id) throws Exception;


    int count() throws Exception;

    int countByUserId(@Nullable String userId) throws Exception;

    void remove(@Nullable Project model) throws Exception;

    void removeByUserId(@Nullable String userId, @Nullable Project model) throws Exception;

    void removeById(@Nullable String id) throws Exception;

    void removeByUserIdAndId(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    Project update(@Nullable Project model) throws Exception;

    @Nullable
    Project updateByUserId(@Nullable String userId, @Nullable Project model) throws Exception;

    @Nullable
    Project updateById(
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    @Nullable
    Project updateByUserIdAndId(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

}
