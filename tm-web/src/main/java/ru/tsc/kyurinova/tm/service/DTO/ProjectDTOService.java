package ru.tsc.kyurinova.tm.service.DTO;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kyurinova.tm.api.service.dto.IProjectDTOService;
import ru.tsc.kyurinova.tm.dto.model.ProjectDTO;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.exeption.empty.EmptyFieldException;
import ru.tsc.kyurinova.tm.exeption.empty.EmptyIdException;
import ru.tsc.kyurinova.tm.exeption.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.repository.DTO.ProjectDTORepository;

import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class ProjectDTOService implements IProjectDTOService {

    @Nullable
    @Autowired
    private ProjectDTORepository repository;

    @Override
    @Transactional
    public @NotNull ProjectDTO add(@NotNull ProjectDTO model) throws Exception {
        return repository.save(model);
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO addByUserId(@Nullable final String userId, @NotNull final ProjectDTO model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyFieldException("UserId");
        model.setUserId(userId);
        return repository.save(model);
    }

    @Override
    @Transactional
    public void changeProjectStatusById(
            @Nullable final String id,
            @Nullable final Status status
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyFieldException("Status");
        @Nullable final ProjectDTO project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        repository.save(project);
    }

    @Override
    @Transactional
    public void changeProjectStatusByUserIdAndId(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyFieldException("UserId");
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyFieldException("Status");
        @Nullable final ProjectDTO project = findOneByUserIdAndId(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        repository.save(project);
    }

    @Override
    @Transactional
    public void clear() throws Exception {
        repository.deleteAll();
    }

    @Override
    @Transactional
    public void clearByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyFieldException("UserId");
        repository.deleteByUserId(userId);
    }

    @Override
    public int count() throws Exception {
        return (int) repository.count();
    }

    @Override
    public int countByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyFieldException("UserId");
        return (int) repository.countByUserId(userId);
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO create(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyFieldException("UserId");
        if (name == null || name.isEmpty()) throw new EmptyFieldException("Name");
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        return repository.save(project);
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyFieldException("UserId");
        if (name == null || name.isEmpty()) throw new EmptyFieldException("Name");
        if (description == null || description.isEmpty()) throw new EmptyFieldException("Description");
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        return repository.save(project);
    }


    @Override
    public boolean existsById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    public boolean existsByUserIdAndId(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyFieldException("UserId");
        if (id == null || id.isEmpty()) return false;
        return repository.existByUserIdAndId(userId, id);
    }


    @Override
    public @Nullable List<ProjectDTO> findAll() throws Exception {
        return repository.findAll();
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAllByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyFieldException("UserId");
        return repository.findByUserId(userId);
    }


    @Override
    public @Nullable ProjectDTO findOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        @NotNull final Optional<ProjectDTO> result = repository.findById(id);
        return result.orElse(null);
    }

    @Nullable
    @Override
    public ProjectDTO findOneByUserIdAndId(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyFieldException("UserId");
        if (id == null || id.isEmpty()) return null;
        @NotNull final Optional<ProjectDTO> result = repository.findByUserIdAndId(userId, id);
        return result.orElse(null);
    }


    @Override
    @Transactional
    public void remove(@Nullable ProjectDTO model) throws Exception {
        if (model == null) throw new ProjectNotFoundException();
        repository.delete(model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException("Project");
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public void removeByUserId(@Nullable final String userId, @Nullable final ProjectDTO model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyFieldException("UserId");
        if (model == null) return;
        if (findOneByUserIdAndId(model.getUserId(), model.getId()) == null) return;
        repository.delete(model);
    }

    public void removeByUserIdAndId(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyFieldException("UserId");
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void changeProjectDTOStatusById(@Nullable String id, @Nullable Status status) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException("Project");
        if (status == null) throw new EmptyFieldException("Status");
        @Nullable final ProjectDTO project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        repository.save(project);
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDTO update(@Nullable final ProjectDTO model) throws Exception {
        if (model == null) return null;
        return repository.save(model);
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDTO updateByUserId(@Nullable final String userId, @Nullable final ProjectDTO model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyFieldException("UserId");
        if (model == null) return null;
        if (findOneByUserIdAndId(model.getUserId(), model.getId()) == null) return null;
        model.setUserId(userId);
        repository.save(model);
        return model;
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDTO updateById(
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyFieldException("Name");
        if (description == null || description.isEmpty()) throw new EmptyFieldException("Description");
        @Nullable final ProjectDTO project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        repository.save(project);
        return project;
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDTO updateByUserIdAndId(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyFieldException("UserId");
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyFieldException("Name");
        if (description == null || description.isEmpty()) throw new EmptyFieldException("Description");
        @Nullable final ProjectDTO project = findOneByUserIdAndId(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        repository.save(project);
        return project;
    }

}
