package ru.tsc.kyurinova.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;
import ru.tsc.kyurinova.tm.enumerated.Status;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tm_task")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "taskDto", propOrder = {
        "id",
        "name",
        "description",
        "status",
        "projectId",
        "created",
        "startDate",
        "finishDate",
        "createdBy",
        "updated",
        "updatedBy",
        "userId"
})
@EntityListeners(AuditingEntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class TaskDTO {

    private static final long serialVersionUID = 1;

    @Id
    @NotNull
    @Column(name = "row_id")
    @XmlElement(required = true)
    protected String id = UUID.randomUUID().toString();

    public TaskDTO(@NotNull String name) {
        this.name = name;
    }

    public TaskDTO(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    public TaskDTO(@NotNull String name, @NotNull String description, @Nullable Date startDate) {
        this.name = name;
        this.description = description;
        this.startDate = startDate;
    }

    @NotNull
    @Column
    @XmlElement(required = true)
    private String name = "";

    @NotNull
    @Column(name = "descr")
    @XmlElement(required = true)
    private String description = "";

    @NotNull
    @Column
    @Enumerated(EnumType.STRING)
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "project_id")
    @XmlElement(required = true)
    private String projectId = null;

    @Nullable
    @Column(name = "start_dt")
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    @Nullable
    @Column(name = "finish_dt")
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date finishDate;

    @NotNull
    @Column
    @CreatedDate
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date created;

    @Column(name = "created_by")
    @NotNull
    @CreatedBy
    private String createdBy;

    @Column
    @NotNull
    @LastModifiedDate
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date updated;

    @Column(name = "updated_by")
    @NotNull
    @LastModifiedBy
    private String updatedBy;

    @Nullable
    @Column(name = "user_id")
    private String userId;

}