package ru.tsc.kyurinova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.model.Session;

import java.util.Optional;

public interface ISessionService extends IService<Session> {

    @NotNull
    Session open(@NotNull String login, @NotNull String password);

    @NotNull
    Session sign(@NotNull Session session);

    void close(@Nullable Session session);

    @NotNull
    boolean checkDataAccess(@NotNull String login, @NotNull String password);

    boolean exists(@NotNull String sessionId);

    void validate(@NotNull Session session);

    void validate(@NotNull Session session, @NotNull Role role);

    void remove(@Nullable Session entity);

    Optional<Session> findById(@Nullable String id);

    @NotNull
    Session findByIndex(@Nullable Integer index);

    void removeById(@Nullable String id);

    void removeByIndex(@Nullable Integer index);

    int getSize();
}
