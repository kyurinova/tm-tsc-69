package ru.tsc.kyurinova.tm.enumerated;

import ru.tsc.kyurinova.tm.comparator.ComparatorByCreated;
import ru.tsc.kyurinova.tm.comparator.ComparatorByName;
import ru.tsc.kyurinova.tm.comparator.ComparatorByStartDate;
import ru.tsc.kyurinova.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

public enum Sort {

    NAME("Sort by name", ComparatorByName.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance()),
    START_DT("Sort by start date", ComparatorByStartDate.getInstance()),
    CREATED("Sort by created", ComparatorByCreated.getInstance());

    private final String displayName;

    private final Comparator comparator;

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

}
