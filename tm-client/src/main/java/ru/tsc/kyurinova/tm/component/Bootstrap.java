package ru.tsc.kyurinova.tm.component;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.api.service.*;
import ru.tsc.kyurinova.tm.event.ConsoleEvent;
import ru.tsc.kyurinova.tm.listener.AbstractListener;
import ru.tsc.kyurinova.tm.constant.TerminalConst;
import ru.tsc.kyurinova.tm.endpoint.*;
import ru.tsc.kyurinova.tm.service.*;
import ru.tsc.kyurinova.tm.util.SystemUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;


@Getter
@Setter
@Component
public final class Bootstrap {

    @Nullable
    @Autowired
    private AbstractListener[] listeners;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ILogService logService;

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    @Getter
    @NotNull
    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    public void start(@Nullable final String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        runArgs(args);
        initPID();
        fileScanner.init();
        logService.debug("Test environment.");
        adminUserEndpoint.createAdminUser("admin", "admin");
        @NotNull final Scanner scanner = new Scanner(System.in);
        @NotNull String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            try {
                System.out.println("ENTER COMMAND:");
                command = scanner.nextLine();
                logService.command(command);
                runCommand(command);
                logService.info("Completed");
            } catch (final Exception e) {
                logService.error(e);
            }
        }
    }

    private void runArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        @NotNull final String arg = args[0];
        @Nullable final AbstractListener listener = getListenerByArg(arg);
        if (listener != null) publisher.publishEvent(new ConsoleEvent(listener.command()));
    }

    private AbstractListener getListenerByArg(@NotNull final String arg) {
        for (@NotNull final AbstractListener listener : listeners) {
            if (arg.equals(listener.arg())) return listener;
        }
        return null;
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public void runCommand(@Nullable final String command) {
        if (command == null || command.isEmpty()) return;
        publisher.publishEvent(new ConsoleEvent(command));
    }

}
